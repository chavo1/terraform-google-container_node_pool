resource "null_resource" "MultiHelloAzure" {
  count = var.multiple
  provisioner "local-exec" {
    command = "echo Hello Azure ${count.index + 1}!"
  }
}